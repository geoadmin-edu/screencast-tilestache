<h5>Tilestache - a Python-based application server</h5>
-
Este projeto serve de base para o screencast sobre Tilestache, que foi gravado e está disponível neste [link](https://www.youtube.com/watch?v=q6fzhsiCjog&feature=em-share_video_user), 
com a apresentação neste [link](http://slides.com/phillipepereira/deck#/).

A apresentação contém os passos necessários para instalação do ambiente e exemplos de configuração para servir e consumir os serviços do Tilestache.